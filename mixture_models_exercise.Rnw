
\documentclass[10pt]{beamer}
\usepackage[T1]{fontenc}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}
\setbeamertemplate{itemize items}[default]
\setbeamertemplate{enumerate items}[default]
\setbeamertemplate{navigation symbols}{}
%\setbeamerfont{subsection in toc}{size=\small}
\usepackage{url}
%\usepackage{tikz}
\usepackage{amsmath} 
%\usetheme{Warsaw}
\usecolortheme{rose}
\begin{document}
% \SweaveOpts{concordance=TRUE}

<<setup, include=FALSE>>=
# size = c("normalsize", "tiny", "scriptsize",
# "footnotesize", "small", "large", "Large", "LARGE", "huge", "Huge")
opts_chunk$set(comment=NA, fig.width=3.5, fig.height=3.5, dev='tikz', fig.align='center', message=FALSE, warning=FALSE)
options(width=60) # make the printing fit on the page
@

\title{12.5 Mixture Models}
\author[]{Peter Str\"{o}m}
\maketitle




\begin{frame}[fragile]{}

<<echo=FALSE, message=FALSE, warning=FALSE, fig.height=3, fig.width=4>>=
require(mvtnorm)
y<- scan('geyser.dat')

## TWO UNIVARIATE NORMALS
#starting values
p<- 0.3
mu1<- 55
sig1<- 4
mu2<- 80
sig2<- 7

for (i in 1:25){
  deny <- p*dnorm(y,mu1,sig1) +  (1-p)*dnorm(y,mu2,sig2)
  p1 <-  p*dnorm(y,mu1,sig1)/deny
  p2 <-  1- p1
  
  # updates:
  p <- sum(p1)/length(p1)
  mu1 <- sum(p1*y)/sum(p1)
  mu2 <- sum(p2*y)/sum(p2)
  sig11 <- sum(p1 *(y-mu1)^2)/ sum(p1); sig1<- sqrt(sig11)
  sig22 <- sum(p2 *(y-mu2)^2)/ sum(p2); sig2<- sqrt(sig22)
}

x<- seq(min(y),max(y),len=100)
den <- p*dnorm(x,mu1,sig1) + (1-p)*dnorm(x,mu2,sig2)
plot(x,den, "l", xlab = NA, ylab=NA, axes=FALSE, mar=rep(0, 4))
@


\begin{align*}
        p_{\theta}(u) &=\sum_{j=1}^J  \pi_jp_j(u | \theta_j), \text{  where the } \pi_j\text{'s sum to 1.}
\end{align*}

\end{frame}




\begin{frame}[fragile]{}
Given some data $y = (y_1, \dots , y_n)$ we can write the likelihood as

\begin{align*}
        \text{log }L(\theta ; y) &=\sum_i  \text{log } \left\{\sum_{j=1}^J  \pi_jp_j(y_i | \theta_j)\right\}
\end{align*}
\end{frame}





\begin{frame}[fragile]{}
<<echo=FALSE, size='footnotesize'>>=
print(y)
@
\end{frame}




\begin{frame}[fragile]{}
<<plot1, echo=FALSE>>=
hist(y,nclass=20,prob=T,main='',
        xlab='Waiting time (minutes)',
        ylab='density',#inside=F,style='old',
        ylim=c(0,.05))

# x<- seq(min(y),max(y),len=100)
# den <- p*dnorm(x,mu1,sig1) + (1-p)*dnorm(x,mu2,sig2)
# lines(x,den)
@
\end{frame}





\begin{frame}{}
We model the data as coming from a normal mixture
\begin{align*}
        \pi_1\text{N}(\mu_1, \sigma_1) + \pi_2\text{N}(\mu_2, \sigma_2)
\end{align*}
Here $\pi_2 = 1 - \pi_1$ so $\theta = (\pi_1, \mu_1, \sigma_1, \mu_2, \sigma_2)$.
\end{frame}




\begin{frame}{EM approach}
\begin{enumerate}
\item Define 'complete data' $x = (x_1, \dots , x_n)$, where $x_i = (y_i, z_i)$.
\item The marginal probability of $Z_i$ is $P(Z_i = j) = \pi_j$.
\item Conditional on $z_i = j$ assume $y_i$ has density $p_j(u|\theta_j)$. 
\end{enumerate}

\begin{align*}
        \text{log }L(\theta ; x_i) &= \text{log }p_{z_i}(y_i|\theta_{z_i}) + \text{log } \pi_{z_i}\\
        &=\sum_{j=1}^J \left\{I(z_i = j) \text{log }p_j(y_i|\theta_j) + I(z_i = j) \text{log }\pi_j \right\}
\end{align*}
\end{frame}




\begin{frame}{E-step}
We can estimate the probability of $y_i$ coming from population j.

\begin{align*}
        \hat{p}_{ij} &= \text{E}\left\{I(Z_i = j)|y_i, \theta^0 \right\}\\
        &=P(Z_i = j|y_i, \theta^0)\\
        &=\frac{\pi_j^0p_j(y_i|\theta_j^0)}{p_{\theta^0}(y_i)}\\
        &=\frac{\pi_j^0p_j(y_i|\theta_j^0)}{\sum_k\pi_k^0p_k(y_i|\theta_k^0)}\\
\end{align*}
\end{frame}




\begin{frame}{M-step}
Update the parameters of each component \textit{separately} by maximizing the weighted log-likelihood
$$\sum_i \hat{p}_{ij}\text{log }p_j(y_i|\theta_j).$$

For the mixing probabilities we have the updating formula
$$\pi_j^1 = \frac{\sum_i\hat{p}_{ij}}{n}.$$
\end{frame}





\begin{frame}{Example: Old Faithful cont'd}
Assuming a mixture of two normal distributions for the Old Faithful data, we have $\theta = (\pi_1, \mu_1, \sigma_1, \mu_2, \sigma_2)$ and $\hat{\pi}_2 = 1 - \hat{\pi}_1$.
\begin{enumerate}
\item Make an initial guess of the parameters and find the $\hat{p}_{ij}$ according to the E-step formula.
\item Use the $\hat{p}_{ij}$ to update the parameters for each component using the weighted likelihood
$$-\frac{1}{2}\sum_i \hat{p}_{ij}\left\{ \text{log }\sigma_j^2 + \frac{(y_i - \mu_j)^2}{\sigma_j^2}\right\}$$
\item Repeat. 
\end{enumerate}
\end{frame}




\begin{frame}{}
In this case we get closed-form update formulas as
\begin{align*}
        \mu_j^1 &= \frac{\sum_i \hat{p}_{ij}y_i}{\sum_i \hat{p}_{ij}}\\
        \sigma_j^{2(1)}&= \frac{\sum_i \hat{p}_{ij}(y_i- \mu_j^1)^2}{\sum_i \hat{p}_{ij}}.
\end{align*}
\end{frame}




\begin{frame}[fragile]{}
The algorithm parameters converge to 
$(\hat{\pi}_1 = 0.308, \hat{\mu}_1 = 54.203, \hat{\sigma}_1 = 4.952, \hat{\mu}_2 = 80,360, \hat{\sigma}_2 = 7,508),$ giving the density estimate $\hat{p}(u) = \hat{\pi}_1\phi(u, \hat{\mu}_1, \hat{\sigma}_1^2) + (1 - \hat{\pi}_1)\phi(u, \hat{\mu}_2, \hat{\sigma}_2^2)$
<<plot3, echo=FALSE, fig.height=3, fig.width=3>>=
hist(y,nclass=20,prob=T,main='',
        xlab='Waiting time (minutes)',
        ylab='density',#inside=F,style='old',
        ylim=c(0,.05))

x<- seq(min(y),max(y),len=100)
den <- p*dnorm(x,mu1,sig1) + (1-p)*dnorm(x,mu2,sig2)
lines(x,den)
@

\end{frame}




\begin{frame}[fragile]{Exercise}
<<plot_e1, echo=FALSE>>=
   y1 <- y[-1]
 data <- data.frame(t1 = y[-299], t = y1)
 plot(data$t, data$t1, ylab = "prev. waiting time", xlab = "waiting time")
@
  

\end{frame}




\begin{frame}[fragile]{}

<<eval=FALSE, size='tiny'>>=
   #starting values
 p1 <- 0.2; p2 <- 0.3
 mu11 <- 80; mu12 <- 55; sig11 <- 10; sig12 <- 10; sig1r <- 0
 mu21 <- 80; mu22 <- 80; sig21 <- 10; sig22 <- 10; sig2r <- 0
 mu31 <- 55; mu32 <- 85; sig31 <- 10; sig32 <- 10; sig3r <- 0

 mu1 <- c(mu11, mu12); mu2 = c(mu21, mu22); mu3 = c(mu31, mu32)
 sig1 <- cbind(c(sig11, sig1r), c(sig1r, sig12))
 sig2 <- cbind(c(sig21, sig2r), c(sig2r, sig22))
 sig3 <- cbind(c(sig31, sig3r), c(sig3r, sig32))

 for (i in 1:25){
     deny <- p1*dmvnorm(x = cbind(data$t1, data$t), mean = mu1, sigma = sig1) +
       p2*dmvnorm(x = cbind(data$t1, data$t), mean = mu2, sigma = sig2) +
         (1 - p1 - p2)*dmvnorm(x = cbind(data$t1, data$t), mean = mu3, sigma = sig3)
  
     pr1 <-  p1*dmvnorm(x = cbind(data$t1, data$t), mean = mu1, sigma = sig1) / deny
     pr2 <-  p2*dmvnorm(x = cbind(data$t1, data$t), mean = mu2, sigma = sig2) / deny
     pr3 <- 1 - pr1 - pr2
  
     # updates:
     p1 <- sum(pr1) / length(pr1);  p2 <- sum(pr2) / length(pr2)
     mu11 <- sum(pr1*data$t1) / sum(pr1);  mu12 <- sum(pr1*data$t) / sum(pr1)
     mu21 <- sum(pr2*data$t1) / sum(pr2);  mu22 <- sum(pr2*data$t) / sum(pr2)
     mu31 <- sum(pr3*data$t1) / sum(pr3);  mu32 <- sum(pr3*data$t) / sum(pr3)
  
     sig11 <- sum(pr1*(data$t1 - mu11)^2) / sum(pr1); sig12 <- sum(pr1*(data$t - mu12)^2) / sum(pr1)
     sig1r <- sum(pr1*(data$t1 - mu11)*(data$t - mu12)) / sum(pr1)
     sig21 <- sum(pr2*(data$t1 - mu21)^2) / sum(pr2); sig22 <- sum(pr2*(data$t - mu22)^2) / sum(pr2)
     sig2r <- sum(pr2*(data$t1 - mu21)*(data$t - mu22)) / sum(pr2)
     sig31 <- sum(pr3*(data$t1 - mu31)^2) / sum(pr3); sig32 <- sum(pr3*(data$t - mu32)^2) / sum(pr3)
     sig3r <- sum(pr3*(data$t1 - mu31)*(data$t - mu32)) / sum(pr3)
  
     mu1 <- c(mu11, mu12); mu2 = c(mu21, mu22); mu3 = c(mu31, mu32)
     sig1 <- cbind(c(sig11, sig1r), c(sig1r, sig12))
     sig2 <- cbind(c(sig21, sig2r), c(sig2r, sig22))
     sig3 <- cbind(c(sig31, sig3r), c(sig3r, sig32))
   }
@
\end{frame}




\begin{frame}[fragile]{}
<<plot_e2, echo=FALSE>>=
  plot(data$t, data$t1, ylab = "prev. waiting time", xlab = "waiting time")

 #starting values
 p1 <- 0.3
 p2 <- 0.3
 mu11 <- 80
 mu12 <- 55
 sig11 <- 10
 sig12 <- 10
 sig1r <- 0
 mu21 <- 80
 mu22 <- 80
 sig21 <- 10
 sig22 <- 10
 sig2r <- 0
 mu31 <- 55
 mu32 <- 85
 sig31 <- 10
 sig32 <- 10
 sig3r <- 0

 mu1 <- c(mu11, mu12); mu2 = c(mu21, mu22); mu3 = c(mu31, mu32)
 sig1 <- cbind(c(sig11, sig1r), c(sig1r, sig12))
 sig2 <- cbind(c(sig21, sig2r), c(sig2r, sig22))
 sig3 <- cbind(c(sig31, sig3r), c(sig3r, sig32))

 for (i in 1:25){
   deny <- p1*dmvnorm(x = cbind(data$t1, data$t), mean = mu1, sigma = sig1) +
       p2*dmvnorm(x = cbind(data$t1, data$t), mean = mu2, sigma = sig2) +
     (1 - p1 - p2)*dmvnorm(x = cbind(data$t1, data$t), mean = mu3, sigma = sig3)

   pr1 <-  p1*dmvnorm(x = cbind(data$t1, data$t), mean = mu1, sigma = sig1) / deny
   pr2 <-  p2*dmvnorm(x = cbind(data$t1, data$t), mean = mu2, sigma = sig2) / deny
   pr3 <- 1 - pr1 - pr2

   # updates:
   p1 <- sum(pr1) / length(pr1)
   p2 <- sum(pr2) / length(pr1)
   mu11 <- sum(pr1*data$t1) / sum(pr1)
   mu12 <- sum(pr1*data$t) / sum(pr1)
   mu21 <- sum(pr2*data$t1) / sum(pr2)
   mu22 <- sum(pr2*data$t) / sum(pr2)
   mu31 <- sum(pr3*data$t1) / sum(pr3)
   mu32 <- sum(pr3*data$t) / sum(pr3)

   sig11 <- sum(pr1*(data$t1 - mu11)^2) / sum(pr1)
   sig12 <- sum(pr1*(data$t - mu12)^2) / sum(pr1)
   sig1r <- sum(pr1*(data$t1 - mu11)*(data$t - mu12)) / sum(pr1)

   sig21 <- sum(pr2*(data$t1 - mu21)^2) / sum(pr2)
   sig22 <- sum(pr2*(data$t - mu22)^2) / sum(pr2)
   sig2r <- sum(pr2*(data$t1 - mu21)*(data$t - mu22)) / sum(pr2)

   sig31 <- sum(pr3*(data$t1 - mu31)^2) / sum(pr3)
   sig32 <- sum(pr3*(data$t - mu32)^2) / sum(pr3)
   sig3r <- sum(pr3*(data$t1 - mu31)*(data$t - mu32)) / sum(pr3)
   
   # sig11 <- sum(pr1*(data$t1 - mu11)^2) / sum(pr1) /(1-sum((pr1/sum(pr1))^2))
   # sig12 <- sum(pr1*(data$t - mu12)^2) / sum(pr1) /(1-sum((pr1/sum(pr1))^2))
   # sig1r <- sum(pr1*(data$t1 - mu11)*(data$t - mu12)) / sum(pr1) /(1-sum((pr1/sum(pr1))^2))
   # 
   # sig21 <- sum(pr2*(data$t1 - mu21)^2) / sum(pr2) /(1-sum((pr2/sum(pr2))^2))
   # sig22 <- sum(pr2*(data$t - mu22)^2) / sum(pr2) /(1-sum((pr2/sum(pr2))^2))
   # sig2r <- sum(pr2*(data$t1 - mu21)*(data$t - mu22)) / sum(pr2) /(1-sum((pr2/sum(pr2))^2))
   # 
   # sig31 <- sum(pr3*(data$t1 - mu31)^2) / sum(pr3) /(1-sum((pr3/sum(pr3))^2))
   # sig32 <- sum(pr3*(data$t - mu32)^2) / sum(pr3) /(1-sum((pr3/sum(pr3))^2))
   # sig3r <- sum(pr3*(data$t1 - mu31)*(data$t - mu32)) / sum(pr3) /(1-sum((pr3/sum(pr3))^2))

   mu1 <- c(mu11, mu12); mu2 = c(mu21, mu22); mu3 = c(mu31, mu32)
   sig1 <- cbind(c(sig11, sig1r), c(sig1r, sig12))
   sig2 <- cbind(c(sig21, sig2r), c(sig2r, sig22))
   sig3 <- cbind(c(sig31, sig3r), c(sig3r, sig32))
   # sig1 <- cov.wt(data, wt = pr1)$cov
   # sig2 <- cov.wt(data, wt = pr2)$cov
   # sig3 <- cov.wt(data, wt = pr3)$cov
 }

 ## Density function EM
 denyMVfun <- function(t1, t){

   fun = p1*dmvnorm(x = cbind(t1, t), mean = mu1, sigma = sig1) +
     p2*dmvnorm(x = cbind(t1, t), mean = mu2, sigma = sig2) +
       (1 - p1 - p2)*dmvnorm(x = cbind(t1, t), mean = mu3, sigma = sig3)
 }

 # Generate contour plot for this density function
 t1 <- seq(40, 100, length.out=100)
 t <- seq(40, 100, length.out=100)
 Z <- outer(X = t, Y = t1, denyMVfun)
 contour(t, t1, Z,  add = TRUE)
@
\end{frame}


%' 
%' \begin{frame}[fragile]{Could we have used \texttt{optim()} instead?}
%' <<echo= FALSE, size='tiny'>>=
%' length(pr1)
%' p1;p2;mu1;mu2;mu3;sig1;sig2;sig3
%' @


% \end{frame}

\begin{frame}[fragile]{Could we have used \texttt{optim()} instead?}
<<echo=FALSE>>=
 #starting values
 p<- 0.3
 mu1<- 55
 sig1<- 4
 mu2<- 80
 sig2<- 7

 ## With EM
 for (i in 1:25){
   deny <- p*dnorm(y,mu1,sig1) +  (1-p)*dnorm(y,mu2,sig2)
   p1 <-  p*dnorm(y,mu1,sig1)/deny
   p2 <-  1- p1

   # updates:
   p <- sum(p1)/length(p1)
   mu1 <- sum(p1*y)/sum(p1)
   mu2 <- sum(p2*y)/sum(p2)
   sig11 <- sum(p1 *(y-mu1)^2)/ sum(p1); sig1<- sqrt(sig11)
   sig22 <- sum(p2 *(y-mu2)^2)/ sum(p2); sig2<- sqrt(sig22)
   # cat(i,round(c(p,mu1,sig1,mu2,sig2),3),'\n')
 }

 ## With optim()
 deny <- function(x) {
     p <- x[1]
     mu1 <- x[2]
     sig1 <-x[3]
     mu2 <- x[4]
     sig2 <- x[5]
  
     fun = -sum(log(p*dnorm(y, mean = mu1 ,sig1) +  (1-p)*dnorm(y,mu2,sig2)))
   }

 opt <- optim(par = c(0.3,55,4,80,7),
                               fn = deny,
                               method = "L-BFGS-B",
                               lower = c(0.2,40,2,60,5),
                               upper = c(0.4,80,6,100,9))

 hist(y,nclass=20,prob=T,main='',
                  xlab='Waiting time (minutes)',
                  ylab='density',#inside=F,style='old',
                  ylim=c(0,.05))
         
          x<- seq(min(y),max(y),len=100)
          den <- p*dnorm(x,mu1,sig1) + (1-p)*dnorm(x,mu2,sig2)
          den_optim <- opt$par[1]*dnorm(x,opt$par[2],opt$par[3]) + (1-opt$par[1])*dnorm(x,opt$par[4],opt$par[5])
          lines(x,den)
          lines(x, den_optim, col = 'red', lty = 'longdash')
@
\end{frame}
         
       
         
         
\begin{frame}[fragile]{Could we have used \texttt{optim()} instead?}
<<echo=FALSE>>=
          # plot(data$t, data$t1, ylab = "prev. waiting time", xlab = "waiting time")
          contour(t, t1, Z)
          ## With optim()
          denyMV <- function(x) {
            p1 <- x[1]
            p2 <- x[2]
            mu11 <- x[3]
            mu12 <- x[4]
            sig11 <-x[5]
            sig12 <- x[6]
            sig1r <- x[7]
            mu21 <- x[8]
            mu22 <- x[9]
            sig21 <- x[10]
            sig22 <- x[11]
            sig2r <- x[12]
            mu31 <- x[13]
            mu32 <- x[14]
            sig31 <- x[15]
            sig32 <- x[16]
            sig3r <- x[17]
         
            mu1 <- c(mu11, mu12); mu2 = c(mu21, mu22); mu3 = c(mu31, mu32)
            sig1 <- cbind(c(sig11, sig1r), c(sig1r, sig12))
            sig2 <- cbind(c(sig21, sig2r), c(sig2r, sig22))
            sig3 <- cbind(c(sig31, sig3r), c(sig3r, sig32))
         
            fun = -sum(log(p1*dmvnorm(x = cbind(data$t1, data$t), mean = mu1, sigma = sig1) +
                             p2*dmvnorm(x = cbind(data$t1, data$t), mean = mu2, sigma = sig2) +
                               (1 - p1 - p2)*dmvnorm(x = cbind(data$t1, data$t), mean = mu3, sigma = sig3)))
          }

 opt <- optim(par = c(0.2, 0.4, 80, 55, 10, 10, 0, 80, 80, 10, 10, 0, 60, 90, 10, 10, 0),
                               fn = denyMV,
                               method = "L-BFGS-B",
                               lower = c(0.1, 0.1 ,70, 40, 1, 1, -9^2, 60, 60, 1, 1, -9^2, 50, 60, 1, 1, -9^2),
                               upper = c(0.5, 0.5, 100, 70, 9^2, 9^2, 9^2, 100, 100, 9^2, 9^2, 9^2, 70, 100, 9^2, 9^2, 9^2))

 ## Density function
 denyMVfun <- function(t1, t){
   x <- opt$par
   p1 <- x[1]
   p2 <- x[2]
   mu11 <- x[3]
   mu12 <- x[4]
   sig11 <-x[5]
   sig12 <- x[6]
   sig1r <- x[7]
   mu21 <- x[8]
   mu22 <- x[9]
   sig21 <- x[10]
   sig22 <- x[11]
   sig2r <- x[12]
   mu31 <- x[13]
   mu32 <- x[14]
   sig31 <- x[15]
   sig32 <- x[16]
   sig3r <- x[17]

   mu1 <- c(mu11, mu12); mu2 = c(mu21, mu22); mu3 = c(mu31, mu32)
   sig1 <- cbind(c(sig11, sig1r), c(sig1r, sig12))
   sig2 <- cbind(c(sig21, sig2r), c(sig2r, sig22))
   sig3 <- cbind(c(sig31, sig3r), c(sig3r, sig32))

   fun = p1*dmvnorm(x = cbind(t1, t), mean = mu1, sigma = sig1) +
       p2*dmvnorm(x = cbind(t1, t), mean = mu2, sigma = sig2) +
     (1 - p1 - p2)*dmvnorm(x = cbind(t1, t), mean = mu3, sigma = sig3)
 }

 # Generate contour plot for this density function
 t1 <- seq(40, 100, length.out=100)
 t <- seq(40, 100, length.out=100)
 Z <- outer(X = t, Y = t1, denyMVfun)
 contour(t, t1, Z, add = TRUE, col = 'red')
@
\end{frame}

%' \begin{frame}[fragile]{}
%' <<>>==
%' opt$par
%' @
%' \end{frame}

\end{document}  

